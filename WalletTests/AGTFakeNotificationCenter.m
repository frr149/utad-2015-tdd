//
//  AGTFakeNotificationCenter.m
//  Wallet
//
//  Created by Fernando Rodríguez Romero on 21/03/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import "AGTFakeNotificationCenter.h"

@interface AGTFakeNotificationCenter ()
@property (nonatomic, strong) NSMutableDictionary *observers;
@end
@implementation AGTFakeNotificationCenter

-(id) init{
    if (self = [super init]) {
        _observers = [NSMutableDictionary dictionary];
    }
    return self;
}

-(void) addObserver:(id) observer
           selector:(SEL) selector
               name:(NSString*) name
             object: (id) observed{
    
    [self.observers setObject:observer
                       forKey:name];
}

-(BOOL) contains:(id) observer
forNotificationName:(NSString*) name{
    
    id o = [self.observers objectForKey:name];
    
    return [observer isEqual: o ];
    
}










@end
