//
//  AGTFakeNotificationCenter.h
//  Wallet
//
//  Created by Fernando Rodríguez Romero on 21/03/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AGTFakeNotificationCenter : NSObject

-(void) addObserver:(id) observer
           selector:(SEL) selector
               name:(NSString*) name
             object: (id) observed;

-(BOOL) contains:(id) observer forNotificationName:(NSString*) name;

@end
