//
//  AGTBrokerTests.m
//  Wallet
//
//  Created by Fernando Rodríguez Romero on 21/03/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "AGTBroker.h"

@interface AGTBrokerTests : XCTestCase
@property (nonatomic, strong) AGTBroker *sut;
@end

@implementation AGTBrokerTests

- (void)setUp {
    [super setUp];
    // Aqui montamos todo el entorno necesario para un test.
    
    self.sut = [AGTBroker new];
    
    [self.sut addRate:2
         fromCurrency:@"USD"
           toCurrency:@"EUR"];
}

- (void)tearDown {
    // Aqui me cargo el chiringuito.
    [super tearDown];
    self.sut = nil;
}


-(void) testDirectRate{
    // pongo una tasa y luego me la devuelve
    
    
    
    XCTAssertEqual(2, [self.sut rateFromCurrency:@"USD" toCurrency:@"EUR"]);
}

-(void) testInverseRate{
    // pongo una tasa y me calcula y devuelve la inversa
    
    XCTAssertEqual(0.5, [self.sut rateFromCurrency:@"EUR"
                                        toCurrency:@"USD"]);
}


-(void) testSelfRate{
    
    XCTAssertEqual(1, [self.sut rateFromCurrency:@"EUR"
                                      toCurrency:@"EUR"]);
    
    XCTAssertEqual(1, [self.sut rateFromCurrency:@"USD"
                                      toCurrency:@"USD"]);

}




@end
