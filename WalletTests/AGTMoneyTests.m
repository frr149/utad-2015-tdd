//
//  AGTMoneyTests.m
//  Wallet
//
//  Created by Fernando Rodríguez Romero on 13/03/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import "AGTMoneyTests.h"
#import "AGTMoney.h"
#import "AGTBroker.h"
#import "AGTWallet.h"

@interface AGTMoneyTests ()
@property(nonatomic, strong) AGTBroker *broker;
@end
@implementation AGTMoneyTests

-(void) setUp{
    [super setUp];
    // Creo un broker
    self.broker = [[AGTBroker alloc] init];
    [self.broker addRate:2 fromCurrency: @"EUR" toCurrency: @"USD"];
}

-(void)tearDown{
    [super tearDown];
    self.broker = nil;
}
-(void) testMultiplication{
    
    AGTMoney *five = [[AGTMoney alloc]initWithAmount: 5 currency:@"USD"];
    AGTMoney *ten = [five times:2];
    
    XCTAssertEqualObjects(ten, [five times:2]);
    XCTAssertEqualObjects([ten times:2], [five times:4]);
    
}

-(void) testEquality{
    
    AGTMoney *five = [[AGTMoney alloc] initWithAmount:5 currency:@"EUR"];
    AGTMoney *product = [five times:2];
    AGTMoney *ten = [[AGTMoney alloc] initWithAmount:10 currency:@"EUR"];
    AGTMoney *tenDollars = [[AGTMoney alloc] initWithAmount:10 currency:@"USD"];

    
    
    XCTAssertEqualObjects(product, ten, @"Objects should be equal with an amount of 10 EUR each");
    XCTAssertNotEqualObjects(product, tenDollars);
    
    XCTAssertNotEqualObjects([[AGTMoney alloc] initWithAmount:1 currency:@"EUR"],
                             [[AGTMoney alloc] initWithAmount:1 currency:@"USD"],
                             @"€1 y $1 no son lo mismo");
}



-(void) testSimpleAddition{
    
    AGTMoney *a = [[AGTMoney alloc] initWithAmount:5 currency:@"USD"];
    AGTMoney *b = [[AGTMoney alloc] initWithAmount:5 currency:@"USD"];
    
    AGTMoney *result = [[AGTMoney alloc]initWithAmount:10 currency:@"USD"];
    
    
    XCTAssertEqualObjects(result, [a plus: b]);
    
    
}

-(void) testReduction{
    
    // 10€ -> 20$, 1:2
    
    
    
    AGTMoney *tenEuro = [[AGTMoney alloc] initWithAmount:10
                                                currency:@"EUR"];
    AGTMoney *twentyDollars = [[AGTMoney alloc] initWithAmount:20
                                                      currency:@"USD"];
    
    
    XCTAssertEqualObjects([tenEuro reduceToCurrency:@"USD" withBroker:self.broker], twentyDollars);
    
}


-(void) testAdditionWithReduction{
    
    // Suma entre distintas divisas
    AGTMoney *w = [[AGTWallet alloc]initWithAmount:10 currency:@"USD"];
    w = [w plus: [[AGTMoney alloc]initWithAmount: 20 currency: @"EUR"]];
    
    // reducimos
    AGTMoney *reduced = [w reduceToCurrency: @"USD" withBroker:self.broker];
    
    
    // Testamos
    XCTAssertEqualObjects([[AGTMoney alloc] initWithAmount:50 currency:@"USD"], reduced);
    
    
}
























@end
