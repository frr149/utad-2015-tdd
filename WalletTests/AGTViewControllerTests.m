//
//  AGTViewControllerTests.m
//  Wallet
//
//  Created by Fernando Rodríguez Romero on 21/03/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "AGTTestViewController.h"
@interface AGTViewControllerTests : XCTestCase

@end

@implementation AGTViewControllerTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}


-(void) testThatTestIsCopiedToLabel{
    
    // Entorno (que será falso)
    UIButton *fakeButton = [[UIButton alloc] init];
    fakeButton.titleLabel.text = @"Hola";
    
    UILabel *fakeLabel  = [[UILabel alloc] init];
    
    // Lo que estamos testando (SUT)
    AGTTestViewController *sut = [[AGTTestViewController alloc] initWithNibName:nil bundle:nil];
    sut.outputLabel = fakeLabel;
    
    // Test
    [sut copyText:fakeButton];
    
    // Asserción
    XCTAssertEqualObjects(fakeLabel.text, fakeButton.titleLabel.text);
    
}
@end















