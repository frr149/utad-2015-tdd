//
//  AGTSingletonTests.m
//  Wallet
//
//  Created by Fernando Rodríguez Romero on 21/03/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "AGTTestViewController.h"
#import "AGTFakeNotificationCenter.h"

@interface AGTSingletonTests : XCTestCase

@end

@implementation AGTSingletonTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}


-(void) testThatObjectSubscribesToBackgroundNotification{
    
    // Entorno (las nalgas falsas)
    AGTFakeNotificationCenter *fn = [AGTFakeNotificationCenter new];
    
    // SUT (loq ue viá testar)
    AGTTestViewController *sut = [[AGTTestViewController alloc] init];
    
    // El método
    [sut susbcribeToBackgroundNotifications:(NSNotificationCenter*)fn];
    
    // La aserción
    XCTAssertTrue([fn contains: sut
           forNotificationName: UIApplicationDidEnterBackgroundNotification], "Se tendría que haber suscrito");
    
    
}















@end
