//
//  AGTBroker.m
//  Wallet
//
//  Created by Fernando Rodríguez Romero on 21/03/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import "AGTBroker.h"

@interface AGTBroker ()
@property (nonatomic, strong) NSMutableDictionary *rates;
@end
@implementation AGTBroker

-(id) init{
    if (self = [super init]) {
        _rates = [NSMutableDictionary dictionary];
    }
    return self;
}

-(void) addRate:(float) rate
   fromCurrency:(NSString*)fromCurrency
     toCurrency:(NSString *) toCurrency{
    
    // directa
    [self.rates setObject:@(rate)
                   forKey:[self keyFromCurrency:fromCurrency
                                     toCurrency:toCurrency]];
    
    // inversa
    [self.rates setObject:@(1/rate)
                   forKey:[self keyFromCurrency:toCurrency
                                     toCurrency:fromCurrency]];
    
    // a si mismo
    [self.rates setObject:@1.0
                   forKey:[self keyFromCurrency:fromCurrency
                                     toCurrency:fromCurrency]];
    [self.rates setObject:@1.0
                   forKey:[self keyFromCurrency:toCurrency
                                     toCurrency:toCurrency]];
}

-(float) rateFromCurrency:(NSString*) fromCurrency
                    toCurrency:(NSString *) toCurrency{
    
    return [[self.rates objectForKey:
             [self keyFromCurrency:fromCurrency
                        toCurrency:toCurrency]] floatValue];
}


#pragma mark - Utils
-(NSString*) keyFromCurrency:(NSString*) fromCurrency
                  toCurrency:(NSString *) toCurrency{
    
    return [NSString stringWithFormat:@"%@->%@", fromCurrency, toCurrency];
}
@end







