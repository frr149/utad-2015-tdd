//
//  AGTMoney.h
//  Wallet
//
//  Created by Fernando Rodríguez Romero on 13/03/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import <Foundation/Foundation.h>

@class AGTBroker;

@interface AGTMoney : NSObject
@property (nonatomic, readonly) NSUInteger amount;
@property (nonatomic, readonly) NSString *currency;

-(id) initWithAmount:(NSUInteger) amount
            currency:(NSString *) currency;

-(AGTMoney*) times:(NSUInteger) multiplier;

-(AGTMoney *) plus: (AGTMoney *) augend;

-(AGTMoney *) reduceToCurrency:(NSString*) toCurrency
                    withBroker:(AGTBroker*) broker;


@end
