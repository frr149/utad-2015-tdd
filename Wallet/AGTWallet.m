//
//  AGTWallet.m
//  Wallet
//
//  Created by Fernando Rodríguez Romero on 21/03/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import "AGTWallet.h"

@interface AGTWallet ()
@property (nonatomic, strong) NSMutableArray *moneys;
@end
@implementation AGTWallet

-(id) init{
    if (self = [super init]) {
        _moneys = [NSMutableArray array];
    }
    return self;
}
-(id)initWithAmount:(NSUInteger)amount
           currency:(NSString *)currency{
    
    if (self = [self init]) {
        AGTMoney *m = [[AGTMoney alloc] initWithAmount:amount
                                              currency:currency];
        [_moneys addObject:m];
    }
    
    return self;
}

-(AGTMoney *) plus:(AGTMoney *)augend{
    
    [self.moneys addObject:augend];
    
    return self;
}


-(AGTMoney *) reduceToCurrency:(NSString *)toCurrency
                    withBroker:(AGTBroker *)broker{
    
    // Array con divisa final
    NSMutableArray *final = [NSMutableArray arrayWithCapacity:self.moneys.count];
    
    // me voy pateando el array de Moneys y los voy reduciendo
    for (AGTMoney *each in self.moneys) {
        
        AGTMoney *newMoney = [each reduceToCurrency:toCurrency
                                         withBroker:broker];
        [final addObject:newMoney];
    }
    
    // Suma sencilla
    AGTMoney *total = [[AGTMoney alloc] initWithAmount:0
                                              currency:toCurrency];
    
    for (AGTMoney *each in final) {
        total = [total plus:each];
    }
    
    return total;
    
}




@end
