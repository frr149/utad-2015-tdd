//
//  AGTMoney.m
//  Wallet
//
//  Created by Fernando Rodríguez Romero on 13/03/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import "AGTMoney.h"
#import "NSObject+GNUstepBase.h"
#import "AGTBroker.h"

@interface AGTMoney ()

@end
@implementation AGTMoney


-(id) initWithAmount:(NSUInteger)amount currency:(NSString *)currency{
    
    if (self = [super init]) {
        _amount = amount;
        _currency = currency;
    }
    
    return self;
}

-(AGTMoney*) times:(NSUInteger)multiplier{
    
    AGTMoney *result = [[AGTMoney alloc]
                        initWithAmount:(self.amount * multiplier) currency:self.currency];
    
    return result;
}

-(AGTMoney *) plus: (AGTMoney *) augend{
    
    AGTMoney *result = [[AGTMoney alloc] initWithAmount:self.amount + augend.amount currency:self.currency];
    return result;
    
}

-(BOOL)isEqual:(id)object{
    
    
    return ((self.amount == [object amount]) &&
            ([self.currency isEqualToString:[object currency]]));
}


-(AGTMoney *) reduceToCurrency:(NSString*) toCurrency withBroker:(AGTBroker*) broker{
    
    // Pedirle al broker la tasas de conversión de mi divisa
    // a la de destino
    float rate = [broker rateFromCurrency:self.currency
                               toCurrency:toCurrency];
    
    // Calcular el nuevo valor
    NSUInteger amount = self.amount * rate;
    
    // Crear una nueva instancia con la divisa de destino
    AGTMoney *reduced = [[AGTMoney alloc] initWithAmount:amount
                                                currency:toCurrency];
    return reduced;
}



@end


