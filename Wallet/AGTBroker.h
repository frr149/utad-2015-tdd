//
//  AGTBroker.h
//  Wallet
//
//  Created by Fernando Rodríguez Romero on 21/03/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AGTBroker : NSObject


-(void) addRate:(float) rate
   fromCurrency:(NSString*)fromCurrency
     toCurrency:(NSString *) toCurrency;

-(float) rateFromCurrency:(NSString*) fromCurrency
                    toCurrency:(NSString *) toCurrency;
@end
