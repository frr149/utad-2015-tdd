//
//  AGTTestViewController.m
//  Wallet
//
//  Created by Fernando Rodríguez Romero on 21/03/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import "AGTTestViewController.h"

@interface AGTTestViewController ()

@end

@implementation AGTTestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)copyText:(id)sender {
    
    self.outputLabel.text = [[sender titleLabel]text];
    
}

-(void) susbcribeToBackgroundNotifications:(NSNotificationCenter*) nc{
    
    [nc addObserver:self
           selector:@selector(didEnterBackground)
               name:UIApplicationDidEnterBackgroundNotification
             object:nil];
    
    // haría más cosas......
    
}

-(void) didEnterBackground{
    
    // deja de usar tanta memoria y tanta batería, jodío
}


@end











