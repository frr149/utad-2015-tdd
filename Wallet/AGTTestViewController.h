//
//  AGTTestViewController.h
//  Wallet
//
//  Created by Fernando Rodríguez Romero on 21/03/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AGTTestViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *outputLabel;
- (IBAction)copyText:(id)sender;


-(void) susbcribeToBackgroundNotifications:(NSNotificationCenter*) nc;


@end
